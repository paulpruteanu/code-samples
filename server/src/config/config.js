module.exports = {
    "development": {
        "dialect": "sqlite",
        "storage": '/tmp/test-dev.db.sqlite'
    },
    "test": {
        "dialect": "sqlite",
        "storage": '/tmp/test-test.db.sqlite'
    },
    "production": {
        "use_env_variable": process.env.DB_HOST
    }
};